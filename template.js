var strTemplateSqlite= `
BEGIN TRANSACTION;
 <% _.each( listaTablas, function( tabla ){ %>
CREATE  TABLE <%- tabla.name %>(
    <% _.each( tabla.atributes, function( atributo, index ){ %><%- atributo.name %> <%- atributo.type %><% if ( atributo.notNull ){ %> NOT NULL <% } %><% if ( atributo.isUnique ){ %> UNIQUE <% } %><% if ( atributo.defaultValue ){ %> DEFAULT  <%- atributo.defaultValue  %> <% } %><% if ( index!=tabla.atributes.length-1 ){ %>,<% } %>
    <% }); %><% if ( tabla.key ){ %>,PRIMARY KEY(<%- tabla.key %>) <% } %> 
    <% _.each( tabla.foreKeys, function( forekey, index ){ %>
    ,FOREIGN KEY(<%- forekey.name %>) REFERENCES <%- forekey.table %>(<%- forekey.forekey %>) <% if ( forekey.onDeleteCascade ){ %> ON DELETE CASCADE <% } %><% if ( forekey.onUpdateCascade ){ %> ON UPDATE CASCADE <% } %>
    <% }); %>
);
<% }); %>
COMMIT;
`
;

// var template = _.template(
//             strTemplate
//         );

// var templateData = [
//     {
//         nombre:"direccion",
//         atributos:[{
//             nombre: "id",
//             tipo: "integer",
//             notNull:true
//         },
//         {
//             nombre: "direccion",
//             tipo: "varchar",
//             notNull:true
//         },
//         {
//             nombre: "persona_id",
//             tipo: "integer",
//             notNull:true
//         }],
//         key:"id",
//         foreKeys:[{
//             nombre: "persona_id",
//             tabla: "persona",
//             forekey:"id"
//         }]
//     },
//     {
//         nombre:"persona",
//         atributos:[{
//             nombre: "id",
//             tipo: "integer",
//             isKey:true
//         },
//         {
//             nombre: "nombre",
//             tipo: "varchar",
//             notNull:true
//         }]
//     }
// ];

// var codigo=template({listaTablas:templateData}).replace(/    \n    \n/g,"").replace(/    \n\);/g,");").replace(/    \n    ,/g,"    ,");

// console.log(codigo);