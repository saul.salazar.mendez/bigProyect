function Rule(){
    this.type="";
    this.min=0;
    this.max=10;
    this.minlength=0;//longitud maxima de cadena
    this.maxLength=0;//longitud minima de cadena
    this.minDate=new Date();
    this.maxDate=new Date();
    this.RegEx="";//la validacion por patrones
}

function addRule(rule){
    this.rules.push(rule);
}

function Atribute() {  
    this.name="";
    this.type="";
    this.len=0;//longitud de campo si es numero o string
    this.rules=[];
    this.notNull=false;
    this.isUnique=false;
}
Atribute.prototype.addRule=addRule;

function ForeKey() {  
    this.name="";
    this.table="";
    this.forekey="";
}

function addAtribute(atribute){
    this.atributes.push(atribute);
}

function addForeKey(foreKey){
    this.foreKeys.push(foreKey);
}

function Table() {
    this.name="";
    this.atributes=[];//un atributo es de la forma 
    this.key="";//de la lista de atributos tiene el que es llave
    this.foreKeys=[]; //{index=indice de atributo; table=Tabla}
    this.url="";//url de los servicios
}
Table.prototype.addAtribute=addAtribute;
Table.prototype.addForeKey=addForeKey;


