/**
 * utils
 * Este archivo tiene las funciones para trabajar con los elementos graficos genericos
 */
(function(){
    var gUtil={
        variables:{
            $gUtil:$("body"),
            $changeLabel:$(".buttonChangeLabel"),
        },
        init:function(){
            this.variables.$gUtil.on("click",".buttonChangeLabel",this.changeLabel.bind(this));
        },
        changeLabel:function(e){
            //icon>button>div
            $padre=$(e.target).parent().parent();
            $label=$padre.children("span");
            bootbox.prompt("Nuevo Valor de la etiqueta", 
                function(result){ 
                    if (result!=null && result!="")
                        $label.text(result); 
            });
        }
    }
    gUtil.init();
})();
function generaJsonTablas(){
            var tablas=$(".tablaER");
            var lista=[];
            for (var kk=0;kk<tablas.length;kk++){
                    var tabla=new Table();
                    tabla.name=$(tablas[kk]).attr("nombre");
                    var atributos=$(tablas[kk]).find("#atributos ul.tabla li");
                    for (var i=0;i<atributos.length;i++){
                        var attributo=new Atribute();
                        attributo.name=$(atributos[i]).attr("nombre");
                        attributo.type=$(atributos[i]).attr("tipo");
                        if ($(atributos[i]).attr("notNull")!="undefined")
                            attributo.notNull=true;
                        tabla.addAtribute(attributo);
                    }
                    if ($(tablas[kk]).find("#key select option:selected").length>0)
                        tabla.key=$(tablas[kk]).find("#key select option:selected").text();
                    var forekeys=$(tablas[kk]).find("#forekeys ul.tabla li");
                    for (var i=0;i<forekeys.length;i++){
                        var attributo=new ForeKey();
                        attributo.name=$(forekeys[i]).attr("atributo");
                        attributo.table=$(forekeys[i]).attr("tabla");
                        attributo.forekey=$(forekeys[i]).attr("forekey");
                        tabla.addForeKey(attributo);
                    }
                    lista.push(tabla);
                }
            return lista;
        }
function downloadFile(data,filename, type){
    var blob = new Blob([data], {type: type});
    if(window.navigator.msSaveOrOpenBlob) {
        window.navigator.msSaveBlob(blob, filename);
    }
    else{
        var elem = window.document.createElement('a');
        elem.href = window.URL.createObjectURL(blob);
        elem.download = filename;        
        document.body.appendChild(elem);
        elem.click();        
        document.body.removeChild(elem);
    }
}