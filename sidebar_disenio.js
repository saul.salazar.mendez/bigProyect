(function(){
    const elementos = {
        btn_default: '<button type="button" class="btn btn-default">Action</button>',
        btn_primary: '<button type="button" class="btn btn-primary">Action</button>',
        btn_success: '<button type="button" class="btn btn-success">Action</button>',
        btn_info: '<button type="button" class="btn btn-info">Action</button>',
        btn_warning: '<button type="button" class="btn btn-warning">Action</button>',
        btn_danger: '<button type="button" class="btn btn-danger">Action</button>',
    }
    var sideBar={
        variables:{
            $body:$("#DISENIO"),
            $sideItems:$(".sideitem"),
            $ContenidoDisenio:$(".ContenidoDisenio")
        },
        init:function(){
            this.variables.$body.load( "html/disenio.html" );
            this.variables.$sideItems.each(e => {
                let item = this.variables.$sideItems[e];
                item.setAttribute('draggable', true);
            });
            this.variables.$sideItems.off('dragstart').on('dragstart', this.drag.bind(this));
            this.variables.$ContenidoDisenio.off("drop").on("drop",this.drop.bind(this));
            this.variables.$ContenidoDisenio.off("dragover").on("dragover",this.allowDrop.bind(this));
            console.log("Modulo sidebar disenio cargado",this.variables);
        },
        drop(ev) {
            ev.preventDefault();
            const key = ev.originalEvent.dataTransfer.getData("elemento");
            if (elementos[key]) {
                $(ev.target).append(elementos[key]);
            }

        },
        drag(ev) {  
            ev.originalEvent.dataTransfer.setData("elemento", $(ev.target).attr("elemento") );
        },
        allowDrop(ev) {
            ev.preventDefault();
        }
        
    }
    sideBar.init();
})();