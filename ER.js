/**
 */
(function(){
    var er={
        init:function(){
            var intVar=function() {
                myVar = setTimeout(myFunction.bind(this), 500);
                function myFunction() {
                    this.variables={
                    $div:$("#ER"),
                        funSqlite: _.template(strTemplateSqlite)
                    };
                    this.variables.$div.off('click');
                    this.variables.$div.off('mouseenter');
                    this.variables.$div.off('mouseleave');
                    this.variables.$div.on('mouseenter','#key select', this.selectAttr.bind(this));
                    this.variables.$div.on('mouseleave','#key select', this.leaveSelect.bind(this));
                    this.variables.$div.on('mouseenter','#forekeys ul li', this.selectForekey.bind(this));
                    this.variables.$div.on('mouseleave','#forekeys ul li', this.leaveForekey.bind(this));
                    this.variables.$div.on("click",".changeNombreTabla",this.changeNombreTabla.bind(this));
                    this.variables.$div.on("click",".addAtributo",this.addAtributo.bind(this));
                    this.variables.$div.on("click",".addForekey",this.addForekey.bind(this));
                    $(".addTableER").off('click').on("click",this.addTable.bind(this));
                    $(".exportSqlite").off('click').on("click",this.exportSqlite.bind(this));
                    clearTimeout(myVar);
                }
            }
            var init2=intVar.bind(this);
            init2();
            
            $("#bs-example-navbar-collapse-6").on('change','#file-input' ,intVar.bind(this) );
        },
        exportSqlite:function(){
            var tablas=generaJsonTablas();
            console.log(tablas);
            var schema = this.variables.funSqlite({listaTablas:tablas}).replace(/    \n    \n/g,"").replace(/    \n\);/g,");").replace(/    \n    ,/g,"    ,");
            downloadFile(schema,"schemaSqlite.sql","text/sql");

        },
        addTable:function(e){
            this.variables.$div.append(
               `
            <!-- tabla -->
             <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <span>nuevaTabla</span>
                            <a href="#" class="changeNombreTabla">
                                <i  class="glyphicon glyphicon-edit"></i>
                            </a>
                        </h3>
                    </div>
                    <div class="panel-body tablaER" nombre="nuevaTabla">
                        <div id="atributos">
                            <div class="pull-right">
                                <sup>Atributos <i class="glyphicon glyphicon-plus-sign addAtributo"></i></sup>
                            </div>
                            <ul class="tabla">
                                
                            </ul>
                        </div>
                        
                        <hr>
                        <div id="key">
                            <div class="pull-right">
                                <sup>key</sup>
                            </div>
                            <select class="form-control">
                                 
                            </select>
                        </div>
                        <hr>
                        <div id="forekeys">
                            <div class="pull-right">
                                <sup>ForeKeys <i class="glyphicon glyphicon-plus-sign addForekey"></i></sup>
                            </div>
                            <ul class="tabla">
                                
                            </ul>
                        </div>
                    </div>
                </div>
             </div><!-- fin tabla -->`

                );
        },
        addForekey:function(e){
            var agregaAtributo=function(atributo,tabla,forekey){
                var padre=$(e.target).parent().parent().parent();
                var tabla2=$(padre).find("ul.tabla");
                $(tabla2).append(`
                    <li class="label label-default" atributo="`+atributo+`" forekey="`+forekey+`" tabla="`+tabla+`">`+atributo+`</li>
                    `);
            };
            var tablas=this.variables.$div.find(".tablaER");
            var padre=$(e.target).parent().parent().parent();
            var localAtributos=$(padre).parent().find("#atributos ul.tabla li");
            var makeOption=function(attr){
                var options="<option disabled selected value> -- select an option -- </option>";
                for (var i=0;i<attr.length;i++){
                    options+="<option>"+$(attr[i]).attr("nombre")+"</option>";
                }
                return options;
            }
            var dialog = bootbox.dialog({
            title: 'Datos de la llave foranea',
            message: `
                <div class="form-group">
                    <label for="atributo">Llave:</label>
                     <select class="form-control" id="atributo">
                                `+makeOption(localAtributos)+`
                            </select>
                    <label for="tabla">Tabla foranea:</label>
                     <select class="form-control" id="tabla">
                                `+makeOption(tablas)+`
                            </select>
                    <label for="forekey">Referencia:</label>
                     <select class="form-control" id="forekey">
                     </select>
                </div>
            `,
            buttons: {
                cancel: {
                    label: "Cancelar",
                    className: 'btn',
                },
                ok: {
                    label: "Agregar",
                    className: 'btn-primary',
                    callback: function(e){
                        var atributo=$(dialog).find("select[id=atributo] option:selected").text(); 
                        var tabla=$(dialog).find("select[id=tabla] option:selected").text(); 
                        var forekey=$(dialog).find("select[id=forekey] option:selected").text(); 
                        agregaAtributo(atributo,tabla,forekey);
                    }
                }
            }
            });
            $(dialog).find("#tabla").change(e=>{
                var tabla=$(dialog).find("#tabla option:selected").text();
                var atr=$("div.tablaER[nombre="+tabla+"]").find("#atributos ul.tabla li");
                var options=makeOption(atr);
                $(dialog).find("#forekey").empty();
                $(dialog).find("#forekey").append(options);
            })
        },
        addAtributo:function(e){
            console.log("sasasasasa");
            var agregaAtributo=function(nombre,tipo,notNull){
                var padre=$(e.target).parent().parent().parent();
                var tabla=$(padre).find("ul.tabla");
                $(tabla).append(`
                    <li nombre="`+nombre+`" tipo="`+tipo+`" notNull="`+notNull+`">
                        <span class="label label-default nombre">`+nombre+`</span>: 
                        <span class="label label-default tipo">`+tipo+`</span>
                        <span ><sup><i class="glyphicon glyphicon-minus-sign removeElement"></i></sup></span>
                    </li>
                    `);
                var select=$(padre).parent().find("div#key select");
                $(select).append("<option>"+nombre+"</option>");
            };
            var dialog = bootbox.dialog({
            title: 'Datos del nuevo atributo',
            message: `
                <div class="form-group">
                    <label for="email">nombre:</label>
                    <input type="text" class="form-control" id="nombre">
                </div>
                <div class="form-group">
                    <label for="tipo">Tipo de dato:</label>
                     <select class="form-control" id="tipo">
                                 <option>INTEGER</option>
                                  <option>REAL</option>
                                  <option>TEXT</option>
                                  <option>NUMERIC</option>
                                  <option>BLOB</option>
                            </select>
                </div>
                <div class="checkbox-inline">
                  <label><input type="checkbox" value="" id="notNull">Not Null</label>
                </div>
            `,
            buttons: {
                cancel: {
                    label: "Cancelar",
                    className: 'btn',
                    callback: function(){
                        
                    }
                },
                ok: {
                    label: "Agregar",
                    className: 'btn-primary',
                    callback: function(e){
                        var nombre=$(dialog).find("input[id=nombre]").val();
                        var tipo=$(dialog).find("select[id=tipo] option:selected").text();
                        var notNull=$(dialog).find("#notNull").attr('checked') 
                        agregaAtributo(nombre,tipo, notNull);
                    }
                }
            }
            });
            
        },
        selectAttr:function(e){
            //icon>button>div
            $padre=$(e.target).parent().parent();
            $atributos=$padre.find("#atributos ul");
            $option=$(e.target).find("option:selected");
            if ($option.text()!=""){
                $li=$atributos.find("li[nombre="+$option.text()+"] span.nombre");
                $li.addClass("label-warning");
            }
            //console.log( $padre,$atributos,$li);
        },
        leaveSelect:function(e){
            //icon>button>div
            $padre=$(e.target).parent().parent();
            $atributos=$padre.find("#atributos ul");           
            if ($option.text()!=""){
                $li=$atributos.find("li span.nombre");
                $li.removeClass("label-warning");
            }
            //console.log( $padre,$atributos,$li);
        },
        selectForekey:function(e){
            //icon>button>div
            $padre=$(e.target).parent().parent().parent();
            tabla=$(e.target).attr("tabla");
            atributo=$(e.target).attr("atributo");
            forekey=$(e.target).attr("forekey");
            
            $atributos=$padre.find("#atributos ul");
            $li=$atributos.find("li[nombre="+atributo+"] span.nombre");
            $li.addClass("label-primary");

            $padreForekey=$(".tablaER[nombre="+tabla+"]");
            $liForekey=$padreForekey.find("#atributos ul li[nombre="+forekey+"] span");
            $liForekey.addClass("label-primary");
           
        },
        leaveForekey:function(e){
            
            $padre=$(e.target).parent().parent().parent();
            tabla=$(e.target).attr("tabla");
            atributo=$(e.target).attr("atributo");
            forekey=$(e.target).attr("forekey");
            
            $atributos=$padre.find("#atributos ul");
            $li=$atributos.find("li span");
            $li.removeClass("label-primary");

            $padreForekey=$(".tablaER[nombre="+tabla+"]");
            $liForekey=$padreForekey.find("#atributos ul li span");
            $liForekey.removeClass("label-primary");
            
        },
        changeNombreTabla:function(e){
            //icon>button>div
            $padre=$(e.target).parent().parent();
            $label=$padre.children("span");
            bootbox.prompt("Nuevo nombre de la tabla", 
                function(result){ 
                    if (result!=null && result!=""){
                        $label.text(result); 
                        $forekeys=$("#ER").find("#forekeys ul li");
                        console.log($forekeys);
                        oldName=$padre.parent().parent().find(".tablaER").attr("nombre");
                        for (var i =0 ; i<$forekeys.length; i++) {
                            console.log(i,$forekeys[i]);
                            if ($($forekeys[i]).attr("tabla")==oldName){
                                $($forekeys[i]).attr("tabla",result);
                            }
                        }
                        $padre.parent().parent().find(".tablaER").attr("nombre",result);

                    }
            });
        }
    }
    er.init();
    console.log("Modulo ER cargado");
})();