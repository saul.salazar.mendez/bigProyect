(function(){
    var menu={
        loadVariables:function(){
            this.variables={
                $menuER:$("#menuER"),
                $submenuER:$(".menuER"),
                $menuDisenio:$("#menuDisenio"),
                $menuServicios:$("#menuServicios"),
                $ER:$("#ER"),
                $DISENIO:$("#DISENIO"),
                $SERVICIOS:$("#SERVICIOS"),
                $mainMenu:$("#bs-example-navbar-collapse-6")
            }
        },
        init:function(){
            this.loadVariables();
            this.variables.$submenuER.off("click").on("click",this.showMenus.bind(this));
            this.variables.$mainMenu.find(".saveProject").off("click").on("click",this.saveProject.bind(this));
            this.variables.$mainMenu.find(".openProject").off("click").on("click",this.openProject.bind(this));
            this.variables.$mainMenu.find("#file-input").off("change").on('change', this.loadProject.bind(this));
            this.variables.$mainMenu.find('#file-input').val("");
        },
        loadProject:function(e){
            if ($(e.target).val()=="")
                return;
            
            var files = e.target.files;
            var obj=this;
            // FileReader support
            if (FileReader && files && files.length) {
                var fr = new FileReader();
                fr.onload = function () {
                    $(".container").html(fr.result);
                    $(e.target).val("");
                    obj.init();
                }
                fr.readAsText(files[0]);
            }
        },
        openProject:function(e){
            console.log("open");
            this.variables.$mainMenu.find('#file-input').trigger('click');
        },
        saveProject:function(e){
            downloadFile($(".container").html(),"projecto.txt","text/html");
        },
        activeDisplay:function(er,disenio,servicios){
            this.variables.$menuER.css("display",er);
            this.variables.$menuDisenio.css("display",disenio);
            this.variables.$menuServicios.css("display",servicios);
            this.variables.$ER.css("display",er);
            this.variables.$DISENIO.css("display",disenio);
            this.variables.$SERVICIOS.css("display",servicios);
        },
        showMenus:function(e){
            var id=e.target.id;
            if (id=="submenuER")
                this.activeDisplay("","none","none");
            if(id=="submenuDisenio")
                 this.activeDisplay("none","","none");
            if(id=="submenuServicios")
                 this.activeDisplay("none","none","");
        }
    };
    menu.init();
})();